package uz.pdp.appoauth2b29.collection;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
@Data
public class User {

    @Id
    private String id;

    @Indexed(unique = true, sparse = true)
    private String phone;

    @Indexed(unique = true, sparse = true)
    private String googleEmail;

    @Indexed(unique = true, sparse = true)
    private String githubUsername;

    private String name;

    private String imageUrl;

    private String password;

    private String role;

    private String provider;
}
