package uz.pdp.appoauth2b29;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AppOauth2B29Application {

    public static void main(String[] args) {
        SpringApplication.run(AppOauth2B29Application.class, args);
    }

}
