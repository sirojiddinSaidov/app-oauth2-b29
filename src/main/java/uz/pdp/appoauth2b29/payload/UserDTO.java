package uz.pdp.appoauth2b29.payload;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;

@Data
public class UserDTO {
    private String id;

    private String phone;

    private String googleEmail;

    private String githubUsername;

    private String name;

    private String imageUrl;

    private String password;

    private String role;

    private String provider;
}
