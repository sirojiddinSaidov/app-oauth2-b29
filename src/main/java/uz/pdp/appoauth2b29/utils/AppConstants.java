package uz.pdp.appoauth2b29.utils;

public interface AppConstants {
    String BEARER_TYPE = "Bearer";
    String BASIC_TYPE = "Basic";
    String AUTH_HEADER = "Authorization";
    String REFRESH_AUTH_HEADER = "RefreshToken";
    String PASSWORD_REGEXP = "^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#_$%^&+=])(?=\\S+$).{8,}$";

    static String QUERY() {
        return "";
    }
}
