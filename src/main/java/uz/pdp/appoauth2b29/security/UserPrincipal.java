package uz.pdp.appoauth2b29.security;

import lombok.Getter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.oauth2.core.user.OAuth2User;
import uz.pdp.appoauth2b29.collection.User;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

@Getter
public class UserPrincipal implements UserDetails, OAuth2User {

    private Map<String, Object> attributes;

    private final User user;

    private final String id;

    private final Collection<? extends GrantedAuthority> authorities;

    private final String password;

    private String username;

    private final boolean accountNonExpired;

    private final boolean accountNonLocked;

    private final boolean credentialsNonExpired;

    private final boolean enabled;

    private UserPrincipal(User user) {
        this.id = user.getId();
        this.authorities = user.getRole() == null ? new ArrayList<>() : List.of(new SimpleGrantedAuthority(user.getRole()));
        this.password = user.getPassword();
        this.username = user.getPhone();
        this.accountNonExpired = true;
        this.accountNonLocked = true;
        this.credentialsNonExpired = true;
        this.enabled = true;
        this.user = user;
    }

    public static UserPrincipal create(User user) {
        return new UserPrincipal(user);
    }

    public static UserPrincipal create(User user, Map<String, Object> attributes) {
        UserPrincipal userPrincipal = UserPrincipal.create(user);
        userPrincipal.attributes = attributes;
        if (userPrincipal.getUsername() == null)
            userPrincipal.username = attributes.get("sub").toString();
        return userPrincipal;
    }

    @Override
    public Map<String, Object> getAttributes() {
        return this.attributes;
    }


    @Override
    public String getName() {
        if (getId() == null)
            return "Qalay";
        return String.valueOf(getId());
    }
}
