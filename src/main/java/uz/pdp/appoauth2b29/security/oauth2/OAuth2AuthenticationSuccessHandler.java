package uz.pdp.appoauth2b29.security.oauth2;

import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.stereotype.Component;
import org.springframework.web.util.UriComponentsBuilder;
import uz.pdp.appoauth2b29.collection.User;
import uz.pdp.appoauth2b29.exceptions.OAuth2AuthenticationProcessingException;
import uz.pdp.appoauth2b29.repository.UserRepository;
import uz.pdp.appoauth2b29.security.JWTProvider;
import uz.pdp.appoauth2b29.security.UserPrincipal;
import uz.pdp.appoauth2b29.utils.CookieUtils;

import java.io.IOException;
import java.util.Objects;
import java.util.Optional;

import static uz.pdp.appoauth2b29.security.oauth2.OAuth2AuthorizationRequestRepository.PROVIDER_COOKIE_NAME;
import static uz.pdp.appoauth2b29.security.oauth2.OAuth2AuthorizationRequestRepository.REDIRECT_URI_PARAM_COOKIE_NAME;


@Component
@RequiredArgsConstructor
public class OAuth2AuthenticationSuccessHandler extends SimpleUrlAuthenticationSuccessHandler {

    private final UserRepository userRepository;
    private final JWTProvider jwtProvider;
    private final OAuth2AuthorizationRequestRepository oAuth2AuthorizationRequestRepository;

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request,
                                        HttpServletResponse response,
                                        Authentication authentication) throws IOException {

        String targetUrl = determineTargetUrl(request, response, authentication);

        if (response.isCommitted()) {
            logger.debug("Response has already been committed. Unable to redirect to " + targetUrl);
            return;
        }

        clearAuthenticationAttributes(request, response);
        getRedirectStrategy()
                .sendRedirect(request, response, targetUrl);
    }

    protected String determineTargetUrl(HttpServletRequest request,
                                        HttpServletResponse response,
                                        Authentication authentication) {
        Optional<String> optionalSignRedirectUri = CookieUtils
                .getCookie(request, REDIRECT_URI_PARAM_COOKIE_NAME)
                .map(Cookie::getValue);


        String provider = CookieUtils
                .getCookie(request, PROVIDER_COOKIE_NAME)
                .map(Cookie::getValue).orElseThrow(OAuth2AuthenticationProcessingException::new);

        String targetUrl = null;
        UserPrincipal userPrincipal = (UserPrincipal) authentication.getPrincipal();
        OAuth2UserInfo oAuth2UserInfo = OAuth2UserInfoFactory
                .getOAuth2UserInfo(provider, userPrincipal.getAttributes());
        if (optionalSignRedirectUri.isPresent())
            targetUrl = optionalSignRedirectUri.orElse(getDefaultTargetUrl());

        if (userPrincipal.getId() == null)
            userPrincipal = registerNewUser(oAuth2UserInfo, provider);

        assert targetUrl != null;

        targetUrl += "?token=" + jwtProvider.createAccessToken(userPrincipal.getId());
        return UriComponentsBuilder.fromUriString(targetUrl)
                .build().toUriString();
    }

    protected void clearAuthenticationAttributes(HttpServletRequest request, HttpServletResponse response) {
        super.clearAuthenticationAttributes(request);
        oAuth2AuthorizationRequestRepository.removeAuthorizationRequestCookies(request, response);
    }


    public UserPrincipal registerNewUser(OAuth2UserInfo oAuth2UserInfo, String provider) {
        User user = new User();
        user.setProvider(provider);
        user.setName(oAuth2UserInfo.getName());

        if (Objects.equals(provider, "google"))
            user.setGoogleEmail(oAuth2UserInfo.getUsername());
        else if (Objects.equals(provider, "github")) {
            user.setGithubUsername(oAuth2UserInfo.getUsername());
        }
        user.setImageUrl(oAuth2UserInfo.getImageUrl());
        return UserPrincipal.create(userRepository.save(user));
    }
}