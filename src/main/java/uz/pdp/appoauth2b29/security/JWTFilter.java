package uz.pdp.appoauth2b29.security;

import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;
import uz.pdp.appoauth2b29.collection.User;
import uz.pdp.appoauth2b29.service.AuthService;
import uz.pdp.appoauth2b29.utils.AppConstants;

import java.io.IOException;
import java.util.Optional;

@Component
@RequiredArgsConstructor
public class JWTFilter extends OncePerRequestFilter {

    private final JWTProvider jwtProvider;
    private final AuthService authService;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        String authorization = request.getHeader("Authorization");
        if (authorization != null) {
            if (authorization.startsWith(AppConstants.BEARER_TYPE))
                setAuthenticationBearer(request, authorization);
        }
        filterChain.doFilter(request, response);
    }


    private void setAuthenticationBearer(HttpServletRequest request, String authorization) {
        String userId = jwtProvider.extractUserId(authorization.substring(AppConstants.BEARER_TYPE.length()).trim());

        Optional<User> optionalUser = authService.findUserById(userId);
        optionalUser.ifPresent(user -> {
            UserPrincipal principal = UserPrincipal.create(user);
            setAuthentication(request, principal);
        });
    }

    private void setAuthentication(HttpServletRequest request, UserPrincipal principal) {
        UsernamePasswordAuthenticationToken authentication =
                new UsernamePasswordAuthenticationToken(
                        principal,
                        null,
                        principal.getAuthorities()
                );

//        //TODO sesion based + token based auth qilsak kerak bo'ladi
//        WebAuthenticationDetails details = new WebAuthenticationDetails(request);
//        authentication.setDetails(details);

        SecurityContextHolder.getContext().setAuthentication(authentication);
    }

}
