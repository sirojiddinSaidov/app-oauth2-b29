package uz.pdp.appoauth2b29.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import uz.pdp.appoauth2b29.collection.User;

import java.util.Optional;

public interface UserRepository extends MongoRepository<User, String> {

    Optional<User> findByGoogleEmail(String googleEmail);

    Optional<User> findByGithubUsername(String githubUsername);
}
