package uz.pdp.appoauth2b29.mapper;

import org.mapstruct.Mapper;
import uz.pdp.appoauth2b29.collection.User;
import uz.pdp.appoauth2b29.payload.UserDTO;

@Mapper(componentModel = "spring")
public interface UserMapper {


    UserDTO toUserDTO(User user);
}
