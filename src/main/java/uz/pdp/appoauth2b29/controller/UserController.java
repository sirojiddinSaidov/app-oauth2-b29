package uz.pdp.appoauth2b29.controller;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import uz.pdp.appoauth2b29.mapper.UserMapper;
import uz.pdp.appoauth2b29.payload.UserDTO;
import uz.pdp.appoauth2b29.security.CurrentUser;
import uz.pdp.appoauth2b29.security.UserPrincipal;

@RestController
@RequestMapping("/api/user")
@RequiredArgsConstructor
public class UserController {

    private final UserMapper userMapper;

    @GetMapping("/me")
    public HttpEntity<UserDTO> userMe(@AuthenticationPrincipal UserPrincipal userPrincipal) {
        UserDTO userDTO = userMapper.toUserDTO(userPrincipal.getUser());
        return ResponseEntity.ok(userDTO);
    }
}
